DROP TABLE IF EXISTS STUDENTS;
DROP TABLE IF EXISTS TEACHERS;
DROP TABLE IF EXISTS ABSENCES;
DROP TABLE IF EXISTS LESSONS;

CREATE TABLE STUDENTS
(
  id varchar(10) NOT NULL PRIMARY KEY,
  lastName varchar(255) NOT NULL,
  firstName varchar(255) NOT NULL
);

CREATE TABLE TEACHERS
(
  id varchar(10) NOT NULL PRIMARY KEY,
  lastName varchar(255) NOT NULL,
  firstName varchar(255) NOT NULL
);

CREATE TABLE LESSONS
(
  id varchar(20) NOT NULL PRIMARY KEY,
  name varchar(255) NOT NULL,
  boss varchar(10) CONSTRAINT fk_data_teachers REFERENCES TEACHERS(id) NOT NULL
);

CREATE TABLE ABSENCES
(
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  student varchar(10) CONSTRAINT fk_data_student REFERENCES STUDENTS(id) NOT NULL,
  lesson varchar(20) CONSTRAINT fk_data_lesson REFERENCES LESSONS(id) NOT NULL,
  teacher varchar(10) CONSTRAINT fk_data_teachers REFERENCES TEACHERS(id) NOT NULL,
  dateStamp varchar(22) NOT NULL
);

INSERT INTO TEACHERS
(`id`, `lastName`, `firstName`)
VALUES
('E197224Z', 'BIDET', 'Emilien');

INSERT INTO STUDENTS
(`id`, `lastName`, `firstName`)
VALUES
('E195194S', 'MATHE', 'Killian'),
('E197447E', 'UNLU', 'Necati'),
('E197225Z', 'GIORGETTI', 'Valentin'),
('E199999Z', 'TEST', 'Hugo'),
('E197228D', 'TEST', 'Warren'),
('E199889Z', 'GRU', 'Antoine'),
('E197224F', 'CARPIO', 'Jules');

INSERT INTO LESSONS
(`id`, `name`, `boss`)
VALUES
('M3106', 'Bases de données avancées', 'E197224Z');
